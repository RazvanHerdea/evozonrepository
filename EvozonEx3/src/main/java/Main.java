import Model.Product;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    static String CSV_LOCATION = "";


    public static void addProduct(ArrayList<Product> pr) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        FileWriter writer = new FileWriter(CSV_LOCATION);
        ColumnPositionMappingStrategy<Product> mappingStrategy = new ColumnPositionMappingStrategy<Product>();
        mappingStrategy.setType(Product.class);

        String[] columns = new String[]
                { "Name", "Price", "Quantity"};
        mappingStrategy.setColumnMapping(columns);

        StatefulBeanToCsvBuilder<Product> builder= new StatefulBeanToCsvBuilder<>(writer);
        StatefulBeanToCsv<Product> beanWriter = builder.withMappingStrategy(mappingStrategy).build();


        beanWriter.write(pr);
        writer.close();

    }




    public static void insert(String name, String price, String quantity) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Product product = new Product(name,price,quantity);
        BufferedReader  br = new BufferedReader(new FileReader(CSV_LOCATION));
        String line = "";
        ArrayList<Product> products = new ArrayList<Product>();


        //read csv file by adding records to arrayList
        try{
            while((line = br.readLine()) != null )
            {
                String[] splitted = line.split(",");
                Product pr = new Product(null,null,null);
                pr.setName(splitted[0]);
                pr.setPrice(splitted[1]);
                pr.setQuantity(splitted[2]);


                products.add(pr);

            }

            products.add(product);
            addProduct(products);

        }catch (Exception e){
            e.printStackTrace();
        }








    }

    public static void readRow() throws IOException {
        BufferedReader  br = new BufferedReader(new FileReader(CSV_LOCATION));
        String line = "";
        List<Product> products = new ArrayList<Product>();
        String columns1 = "Name" + "Price" + "Quantity";



        try{
            while((line = br.readLine()) != null )
            {
                String[] splitted = line.split(",");
                Product pr = new Product(null,null,null);
                pr.setName(splitted[0]);
                pr.setPrice(splitted[1]);
                pr.setQuantity(splitted[2]);


                products.add(pr);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        for(int i=0; i<products.size();i++){
            System.out.println("Product " + i + " =" + "{" + " "
                    + "Name: " + products.get(i).getName()+ ", "  + "Price: "+ products.get(i).getPrice()+ ", " + "Quantity: " + products.get(i).getQuantity() +
                    "}" +"\n");
        }




    }

    public static void deleteProduct(String name) throws FileNotFoundException {
        BufferedReader  br = new BufferedReader(new FileReader(CSV_LOCATION));
        String line = "";
        ArrayList<Product> products = new ArrayList<Product>();
        Product product = new Product(null,null,null);



        try{
            while((line = br.readLine()) != null )
            {
                String[] splitted = line.split(",");
                Product pr = new Product(null,null,null);
                pr.setName(splitted[0]);
                pr.setPrice(splitted[1]);
                pr.setQuantity(splitted[2]);


                products.add(pr);


            }

            for(Product p:products){
                if(p.getName().contains(name)){
                    System.out.println(p.getName());
                   product = p;
                }
            }
            products.remove(product);
            addProduct(products);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public static void editProduct(int index,String name,String price, String quantity) throws FileNotFoundException {


        BufferedReader  br = new BufferedReader(new FileReader(CSV_LOCATION));
        String line = "";
        ArrayList<Product> products = new ArrayList<Product>();
        Product product = new Product(name,price,quantity);



        try{
            while((line = br.readLine()) != null )
            {
                String[] splitted = line.split(",");
                Product pr = new Product(null,null,null);
                pr.setName(splitted[0]);
                pr.setPrice(splitted[1]);
                pr.setQuantity(splitted[2]);


                products.add(pr);


            }
            products.set(index,product);


            addProduct(products);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public static void main(String[] args) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        File f = new File("Test.csv");
        if(!f.exists()){
            f.createNewFile();
        }else{
            System.out.println("File already exists");
            CSV_LOCATION = "C:\\Users\\razva\\Desktop\\Evozon\\EvozonEx3\\Test.csv";
        }

        //insert("dada","10","13");
       // System.out.println("Hello World!");

       // readRow();
       // deleteProduct("dada");

       // editProduct(1,"element","13","22");
    }
}
